/*
 * 
 */
package fibonaccinumber;

import java.util.Scanner;

/**
 *
 * @author hmedcab
 */
public class FibonacciNumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Informamos al usuario de que tiene que introducir un número.
        System.out.print("Introduce un número: ");
        // Instanciamos el scanner para poder leer stdin
        Scanner in = new Scanner(System.in);
        // Guardamos el valor introducido por el usuario en num
        int num = in.nextInt(); 
        // Informamos de que se van a mostrar los números.
        System.out.println("Ahora verás los "+num+" primeros números:");
        // Hacemos un bucle iterativo de 0 hasta num
        for (int i = 0; i < num; i++) {
            // Informamos del resultado de cada iteración.
            System.out.println("El número fibonacci "+i+" es: "+fibo(i));
        }
    }
    
    /**
     * Método estático que devuelve el valor de un número fibonacci.
     * @param index Índice del número de la serie que queremos saber.
     * @return Valor del número fibonacci solicitado.
     */
    public static int fibo(int index){
        int number = 0;
        if(index <= 0) return 0;
        if(index == 1) return 1;
        return fibo(index-1)+fibo(index-2);
    }
}
